#!/usr/bin/env python3
'''
PMS7003 Sensor reading tool
'''
import serial

from sys import stderr, argv

def is_valid(frame):
    s = sum(frame[:-2])
    return len(frame) == 32 and frame[:4] == (66, 77, 0, 28) and frame[-2:] == (s >> 8, s & 0b11111111)

def print_usage():
        print(f"Invalid arguments. Usage:")
        print("\n{argv[0]} <port>")
        print("\nexample:")
        print(f"{argv[0]} /dev/ttyUSB0")
        print(f"{argv[0]} com5")
        
def main():
    if len(argv) != 2:
        print_usage()
        exit(1)

    dev = argv[1]
    try:
        print("=== WHO norm: pm2.5 < 25 ug/m3\t pm10 < 50ug/m3 ===")
        print(f"Reading from PMS7003 sensor on port: {dev}")
            
        with serial.Serial(dev) as ser:
            print("\nPress ctrl-c to exit...\n")
            i = 0
            while True:
                if i % 10 == 0:
                    print("pm1.0 (atm)", "pm2.5 (atm)", "pm10 (atm)", sep='\t')
                i += 1
                frame = tuple(ser.read(32))
                if not is_valid(frame):
                    print("UART read error: Invalid frame", file=stderr)
                    continue
                pm1_st   = (frame[4]  << 8) | frame[5]
                pm25_st  = (frame[6]  << 8) | frame[7]
                pm10_st  = (frame[8]  << 8) | frame[9]
                pm1_atm  = (frame[10] << 8) | frame[11]
                pm25_atm = (frame[12] << 8) | frame[13]
                pm10_atm = (frame[14] << 8) | frame[15]

                print(f"{pm1_st} ({pm1_atm})\t\t{pm25_st} ({pm25_atm})\t\t{pm10_st} ({pm10_atm})", flush=True)

    except KeyboardInterrupt:
        pass
    except serial.serialutil.SerialException:
        print(f"Error: Can't read from port: {dev}. Is sensor connected?", file=stderr)
        exit(2)

if __name__ == '__main__':
    main()
