# Inroduction
This is simple air pollution PM1.0, PM2.5 and PM10 monitoring tool. It reads data the from <a href="https://www.google.com/search?q=PMS7003">PMS7003</a> sensor and prints that on the console.

It can be connected directly to USB port when using <a href="https://botland.store/air-quality-sensors/13445-idc-10pin-127mm-microusb-adapter-for-pms7003-sensor.html">this adapter</a>

# Software requirements
Script requires `pyserial`. This can be installed by typing
```sh
pip3 install -r requirements.txt
```

# Usage examples

### Linux
```sh
python3 pm.py /dev/ttyUSB0
```

### Windows
```
python pm.py comX
```
Where X is the right serial port number

### Mac OS
```sh
./pm.py /dev/tty.usbserial-xxxx
```
Where xxxx is the right serial port number
